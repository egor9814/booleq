//
// Created by egor9814 on 06 Aug 2021.
//

#ifndef BOOLEQ_TESTS_COMMON_HPP
#define BOOLEQ_TESTS_COMMON_HPP

#include <stest.hpp>
#include <pla.hpp>
#include <booleq/d0.hpp>
#include <variant>
#include <fstream>
#include <functional>

#define BoolEqTestUnit(NAME) FixtureTestUnit(NAME, booleq::tests::Fixture)

#define __str0(X) #X
#define __str(X) __str0(X)
#define __defer_cat0(X,Y) X##Y
#define __defer_cat(X,Y) __defer_cat0(X,Y)
#define __defer(NAME) struct NAME { std::function<void()> f; ~NAME() { f(); } } __defer_cat(NAME, __instance); \
	__defer_cat(NAME, __instance).f =
#define defer __defer(__defer_cat(__defer__, __LINE__))

namespace booleq::tests {

	struct Fixture {

		template <typename T>
		class Result {
			std::variant<T, std::string> data;

		public:
			explicit Result(const T &f) : data(f) {}
			explicit Result(const std::string &error) : data(error) {}

			Result(const Result &) = delete;
			Result &operator=(const Result &) = delete;

			[[nodiscard]] bool isError() const {
				return data.index() == 1;
			}

			[[nodiscard]] const T &getResult() const {
				return std::get<T>(data);
			}

			[[nodiscard]] const std::string &getError() const {
				return std::get<std::string>(data);
			}
		};

		[[nodiscard]] Result<pla::File> openPLA(const std::string &path) const {
			try {
				std::ifstream input(path);
				if (!input) throw std::runtime_error("cannot open file: " + path);
				defer [&input] { input.close(); };
				pla::File f{15};
				input >> f;
				return Result(f);
			} catch (const std::exception &err) {
				return Result<pla::File>(err.what());
			} catch (...) {
				return Result<pla::File>("unknown error at: " __FILE__ ":" __str(__LINE__));
			}
		}

		using SolutionsResult = Result<std::vector<booleq::d0::RootFinder::ConjunctionPtr>>;
		[[nodiscard]] SolutionsResult findSolutions(const pla::File &file) const {
			using namespace d0;
			std::vector<RootFinder::ConjunctionPtr> result;
			try {
				for (const auto &[output, dnf] : file.toDNFs()) {
					pla::RawDNF rawDnf{dnf.size(), nullptr};
					auto i = dnf.begin();
					auto end = dnf.end();
					for (auto j = rawDnf.begin(); i != end; ++i, ++j) {
						*j = &(*i);
					}
					auto solution = RootFinder::findRoot(rawDnf, file.inputLabels.size());
					if (RootFinder::checkSolution(rawDnf, solution)) {
						if (auto simplified = RootFinder::trySimplify(rawDnf, solution); simplified) {
							result.push_back(simplified);
						} else {
							result.push_back(solution);
						}
					}
				}
				return SolutionsResult(result);
			} catch (const std::exception &err) {
				return SolutionsResult(err.what());
			} catch (...) {
				return SolutionsResult("unknown error at: " __FILE__ ":" __str(__LINE__));
			}
		}

	};

}

#endif //BOOLEQ_TESTS_COMMON_HPP
