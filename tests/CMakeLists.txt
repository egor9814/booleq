
stest_find_tests(targets "booleq_tests_" "${CMAKE_CURRENT_SOURCE_DIR}" "test_*.cpp" "main.cpp")
foreach(it ${targets})
    plaslib_link_target(${it})
    bitops_link_target(${it})
    booleq_api_link_target(${it})
endforeach()

file(GLOB_RECURSE pla_list RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} "*.pla")
foreach(it ${pla_list})
    configure_file("${CMAKE_CURRENT_SOURCE_DIR}/${it}" "${CMAKE_CURRENT_BINARY_DIR}/${it}" COPYONLY)
endforeach()
