//
// Created by egor9814 on 04 Aug 2021.
//

#include "common.hpp"

BoolEqTestUnit(DNF1)

	AutoTestCase() {
		using namespace bitops::literals;
		static constexpr bool notError = false;
		if (auto r = openPLA("dnf_1.pla"); r.isError()) {
			TestCheckMessage(notError, r.getError());
		} else if (auto s = findSolutions(r.getResult()); s.isError()) {
			TestCheckMessage(notError, s.getError());
		} else {
			const auto &solutions = s.getResult();
			TestCheck(solutions.empty());
		}
	}

EndTestUnit(DNF0)
