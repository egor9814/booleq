//
// Created by egor9814 on 04 Aug 2021.
//

#include "common.hpp"

BoolEqTestUnit(DNF0)

	AutoTestCase() {
		using namespace bitops::literals;
		static constexpr bool notError = false;
		if (auto r = openPLA("dnf_0.pla"); r.isError()) {
			TestCheckMessage(notError, r.getError());
		} else if (auto s = findSolutions(r.getResult()); s.isError()) {
			TestCheckMessage(notError, s.getError());
		} else {
			const auto &solutions = s.getResult();
			TestCheck(solutions.size() == 1);
			TestCheck(*solutions[0] == "0-1--1"_bi);
		}
	}

EndTestUnit(DNF0)
