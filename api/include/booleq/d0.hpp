//
// Created by egor9814 on 06 Aug 2021.
//

#ifndef BOOLEQ_API_D0_HPP
#define BOOLEQ_API_D0_HPP

#include <pla.hpp>
#include <memory>

namespace booleq::d0 {

	/// "Перечисление" "Стратегия ветвления"
	namespace ForkStrategy {
		enum Enum : unsigned char {
			OneZeroX = 0,
			OneXZero,
			ZeroOneX,
			ZeroXOne,
			XOneZero,
			XZeroOne
		};

		static constexpr std::initializer_list<const char *> shortStrings {
			"10-",
			"1-0",
			"01-",
			"0-1",
			"-10",
			"-01"
		};

		Enum parse(const char *shortString, bool *ok = nullptr);
	}

	/// Класс "Искатель корня D0"
	class RootFinder {
	public:
		using ConjunctionPtr = std::shared_ptr<pla::BitInterval>;

		/** Найти корень
		 * @return nullptr, если корень не найден, иначе указатель на найденный корень */
		static ConjunctionPtr findRoot(const pla::RawDNF &dnf, size_t inputsCount, ForkStrategy::Enum forkStrategy);

		/** Упростить корень @param c
		 * @return nullptr, если не удалось упростить, иначе указатель на упрощенный корень */
		static ConjunctionPtr trySimplify(const pla::RawDNF &dnf, const ConjunctionPtr &c);

		/// Проверить, является ли корень @c решением днф @param dnf
		static bool checkSolution(const pla::RawDNF &dnf, const ConjunctionPtr &c);
	};

}

#endif //BOOLEQ_API_D0_HPP
