//
// Created by egor9814 on 17 Aug 2021.
//

#include "impl.hpp"

namespace booleq::d0::impl {

	void RootFinderImpl::fork(size_t var) {
		// применим стратегию ветвления
		forkStrategy->fork(currentBranch, var, branches);
	}

	bool RootFinderImpl::findNext() {
		using namespace algorithms::rules;

		if (!currentBranch) {
			// возьмем ветку из стека, если она не инициализирована
			if (branches.empty())
				// если стек пуст, то решения нет
				return false;
			currentBranch = branches.top();
			branches.pop();
		}
		// получаем текущую переменную для ветвления
		auto currentVar = currentBranch->var;

		while (true) {
			if (currentBranch->fixType == Branch::FixNone) {
				// здесь мы оказываемся, если для ветки не проводился анализ
				if (currentVar == 0) {
					// если рассмотрели все переменные, то берем ветку из стека
					if (branches.empty()) {
						// если стек пуст, то решения нет
						return false;
					} else {
						currentBranch = branches.top();
						branches.pop();
						currentVar = currentBranch->var;
					}
				} else {
					// переходим к следующей переменной
					currentVar--;
					// если в маске присутсвует '1', то по этой компоненте еще не проводился анализ
					if (currentBranch->mask[currentVar].get() != BitInterval::TriBit::One) {
						continue;
					}

					// ветвимся
					fork(currentVar);
				}
			} else {
				// в зависимости от типа фиксирования, применим определенные действия
				switch (currentBranch->fixType) {
					default:
						// по умолчанию, ничего делать не надо
						break;
					case Branch::FixZero:
						// сбрасываем в ноль
						currentBranch->solution[currentBranch->var].reset();
						break;
					case Branch::FixOne:
						// устанавливаем в единицу
						currentBranch->solution[currentBranch->var].set();
						break;
				}

				// применяем первое правило
				if (first(currentBranch->dnf, currentBranch->mask, currentBranch->solution)) {
					return true;
				}

				// применяем второе правило
				if (!second(currentBranch->dnf, currentBranch->mask)) {
					// если отсутсвуют строки из '-', то применяем третье правило
					if (third(currentBranch->dnf, currentBranch->mask, currentBranch->solution)) {
						return true;
					}

					// и наконец четвертое
					if (four(currentBranch->dnf, currentBranch->mask, currentBranch->solution)) {
						return true;
					}
				}

				// сбрасываем тип фиксирования, для дальнейшего анализа
				currentBranch->fixType = Branch::FixNone;
			}
		}
	}

	RootFinderImpl::RootFinderImpl(ForkStrategy::Enum forkStrategy)
			: forkStrategy(ForkStrategyImpl::create(forkStrategy)) {}

	BranchPtr RootFinderImpl::find(const RawDNF &dnf, size_t inputsCount, ForkStrategy::Enum forkStrategy) {
		RootFinderImpl rf{forkStrategy};
		// помещаем в стек "исходную" ветку
		rf.branches.push(Branch::create(dnf, inputsCount));

		// и пытаемся найти решение
		while (rf.findNext()) {
			// по скольку findNext применяет эвристики, сущестует вероятность,
			// что решение будет найдено, но не будет удовлетворять исходной днф,
			// поэтому проверим найденное решение
			if (checkSolution(dnf, rf.currentBranch->solution))
				// все хорошо!
				return rf.currentBranch;
			// иначе, сбрасываем тип фиксирования, для дальнейшего анализа
			rf.currentBranch->fixType = Branch::FixNone;
		}

		// решение не найдено
		return nullptr;
	}

	bool RootFinderImpl::checkSolution(const RawDNF &dnf, const BitInterval &solution) {
		// формируем маску для "отсечения" удаленных столбцов: 111...111
		BitInterval mask;
		BitVector bv{solution.getLength()};
		mask.setInterval(~bv, bv);

		// копируем днф, т.к. нам её придется менять
		auto dnfCopy(dnf);
		// удаляем строки ортогональные к решению
		// и возвращаем true, если dnfCopy опустела, иначе false
		return algorithms::removeOrthogonal(dnfCopy, mask, solution) == 0;
	}
}
