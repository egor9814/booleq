//
// Created by egor9814 on 17 Aug 2021.
//

#ifndef BOOLEQ_D0_IMPL_HPP
#define BOOLEQ_D0_IMPL_HPP

#include <stack>
#include <booleq/d0.hpp>

namespace booleq::d0::impl {

	using bitops::BitVector;
	using bitops::BitInterval;
	using pla::RawDNF;

	/// Структура "Ветка"
	struct Branch;
	using BranchPtr = std::shared_ptr<Branch>;
	struct Branch {
		/// ДНФ
		RawDNF dnf;
		/// Маска
		BitInterval mask;
		/// Решение
		BitInterval solution;
		/// Переменная ветвления
		size_t var;
		/// Тип фиксирования переменной
		enum FixType {
			FixNone = 0, /// не зафиксирована
			FixZero,     /// зафиксирован ноль
			FixOne,      /// зафиксирована единица
			FixX         /// зафиксирован знак '-'
		} fixType;

		/// Зафикисровать переменную
		BranchPtr fix(size_t var, Branch::FixType fixType);

		/// Создать ветку
		static BranchPtr create(const RawDNF &dnf, size_t inputsCount);
	};
	using Branches = std::stack<BranchPtr>;

	/// Интерфейс "Реализация стратегии ветвления"
	struct ForkStrategyImpl {
		virtual ~ForkStrategyImpl() = default;

		virtual void fork(const BranchPtr &branch, size_t var, Branches &branches) = 0;

		static std::shared_ptr<ForkStrategyImpl> create(ForkStrategy::Enum forkStrategy);
	};

	/// Реализация класса "Искатель корня D0"
	class RootFinderImpl {
		/// Стек для ветвей
		Branches branches;

		/// Стратегия ветвления
		std::shared_ptr<ForkStrategyImpl> forkStrategy;

		/// Текущая ветка
		BranchPtr currentBranch;

		/// Создать ответвление
		void fork(size_t var);

		/** Найти возможное решение
		 * @return true, если ветка currentBranch содержит решение, иначе false */
		bool findNext();

		RootFinderImpl(ForkStrategy::Enum forkStrategy);
	public:
		/** Найти корень
		 * @return nullptr, если корень не будет найден, иначе указатель на ветку с найденным корнем */
		static BranchPtr find(const RawDNF &dnf, size_t inputsCount, ForkStrategy::Enum forkStrategy);

		/** Проверить, является ли интервал @returns solution решением
		 * @return true, если является, иначе false */
		static bool checkSolution(const RawDNF &dnf, const BitInterval &solution);
	};

	namespace algorithms {
		/** Ф-ия для удаления ортогональных интервалов из @param dnf
		 * @param dnf ДНФ
		 * @param mask маска для "отсекания" удаленных столбцов
		 * @param result коньюнкция-результат
		 * @return кол-во оставшихся интервалов в ДНФ после удаления ортоганальных к @param result
		 * */
		size_t removeOrthogonal(RawDNF &dnf, BitInterval &mask, const BitInterval &result);

		namespace rules {
			/** Реализация первого правила. Если в матрице имеется строка, где лишь одна компонента отличная
			 *  от значения ‘-’, то, существующий корень будет обладать инверсным значением этой компоненты.
			 * @param dnf ДНФ-матрица
			 * @param mask маска для "отсекания" удаленных столбцов
			 * @param result коньюнкция-результат
			 * @return true, если решение найдено, иначе false
			 * */
			bool first(RawDNF &dnf, BitInterval &mask, BitInterval &result);

			/** Реализация второго правила. Если в матрице имеется строка, все компоненты которой обладают
			 *  значением ‘-’, то корень не существует.
			 * @param dnf ДНФ-матрица
			 * @param mask маска для "отсекания" удаленных столбцов
			 * @return true, если решение НЕ найдено, иначе false
			 * */
			bool second(RawDNF &dnf, BitInterval &mask);

			/** Реализация третьего правила. Если в матрице существует столбец, все компоненты которого имеют
			 *  значение ‘-’, то существующий корень может обладать любым значением по этой компоненте.
			 * @param dnf ДНФ-матрица
			 * @param mask маска для "отсекания" удаленных столбцов
			 * @param result коньюнкция-результат
			 * @return true, если решение найдено, иначе false
			 * */
			bool third(RawDNF &dnf, BitInterval &mask, const BitInterval &result);

			/** Реализация четвертого правила. Если в матрице имеется столбец элементы которого не принимают
			 *  значения ‘0‘,то существующий корень принимает значение ‘0‘ по этой компоненте. Аналогично, если
			 *  элементы столбца не принимают значение ‘1‘, то советующая компонента, вслучае существования корня,
			 *  может иметь значение ‘1‘.
			 * @param dnf ДНФ-матрица
			 * @param mask маска для "отсекания" удаленных столбцов
			 * @param result коньюнкция-результат
			 * @return true, если решение найдено, иначе false
			 * */
			bool four(RawDNF &dnf, BitInterval &mask, BitInterval &result);
		}
	}
}

#endif //BOOLEQ_D0_IMPL_HPP
