//
// Created by egor9814 on 17 Aug 2021.
//

#include "impl.hpp"

namespace booleq::d0::impl::algorithms {

	size_t removeOrthogonal(RawDNF &dnf, BitInterval &mask, const BitInterval &result) {
		size_t count{0};
		// просто идем по всем интервалам
		for (auto &it : dnf) {
			// если строка не удалена
			if (it) {
				// "удаляем" столбцы
				auto v = *it & mask;
				// удаляем строку если она ортогональна к result
				if (v.isOrthogonal(result, false)) {
					it = nullptr;
				} else {
					count++;
				}
			}
		}
		// и теперь хитро обновляем маску (удяляем столбец)
		/*
		 *   mask = -1-11-
		 * result = 101--1
		 *
		 * mask.vec = 010110
		 * mask.dnc = 101001
		 *
		 * result.vec = 101001
		 * result.dnc = 000110
		 *
		 * new mask = ---11-:
		 *  new mask.vec = 000110
		 *  new mask.dnc = 111001
		 *
		 * ~result.dnc = 111001
		 * mask.dnc | ~result.dnc = 101001 | 111001 = 111001
		 *
		 * new mask.dnc = mask.dnc | ~result.dnc = 101001 | 111001 = 111001
		 * new mask.vec = ~new mask.dnc = 000110
		 * new mask = ---11- yes!
		 * */
		auto dnc = mask.getDnc() | ~result.getDnc();
		auto vec = ~dnc;
		mask.setInterval(vec, dnc);
		return count;
	}

	namespace rules {

		bool first(RawDNF &dnf, BitInterval &mask, BitInterval &result) {
			for (const auto &it : dnf) {
				// пропускаем удаленные строки
				if (!it) continue;
				// "удаляем" столбцы
				/*
				 * 000-0-
				 * 010-0-
				 * --010-
				 *
				 * mask = 1-1111
				 *        0-0-0-
				 *        0-0-0-
				 *        --010-
				 *
				 */
				auto i = *it & mask;
				// если в строке всего одна компонента определена, значит ранг интервала будет равен 1
				if (i.getRang() == 1) {
					// добавлем инверсию компоненты в решение
					result |= ~i;
					// и удаляем ортогоналные к решению строки
					if (removeOrthogonal(dnf, mask, result) == 0) {
						// если строк не осталось, значит мы нашли решение
						return true;
					}
					return false;
				}
			}
			return false;
		}

		bool second(RawDNF &dnf, BitInterval &mask) {
			for (const auto &it : dnf) {
				// пропускаем удаленные строки
				if (!it) continue;
				// "удаляем" столбцы
				auto i = *it & mask;
				// если строка будет состоять только из '-', значит ее ранг будет равен 0
				if (i.getRang() == 0)
					// решения нет
					return true;
			}
			return false;
		}

		bool third(RawDNF &dnf, BitInterval &mask, const BitInterval &result) {
			/*
			 * 000-0-
			 * 010-0-
			 * --010-
			 *
			 * bv = 111111
			 * bv = bv & dnc(000-0-) =
			 *    = 111111 & 000101 =
			 *    = 000101
			 * bv = bv & dnc(010-0-) =
			 *    = 000101 & 000101 =
			 *    = 000101
			 * bv = bv & dnc(--010-) =
			 *    = 000101 & 110001 =
			 *    = 000001
			 *
			 * bv = 000001
			 * */
			// создаем вектор, в котором будем хранить столбцы, которые могут принимать любое значение
			auto bv = ~BitVector(mask.getLength());
			// и заполняем его: 1 - любое, 0 - только определенное
			for (const auto &it : dnf) {
				if (!it) continue;
				auto i = *it & mask;
				bv &= i.getDnc();
			}
			// если есть хотя бы одна 1, то "удаляем" из маски заданные столбцы
			if (bv.getWeight()) {
				auto dnc = mask.getDnc() | bv;
				bv ^= mask.getVector();
				mask.setInterval(bv, dnc);
			}
			return removeOrthogonal(dnf, mask, result);
		}

		bool four(RawDNF &dnf, BitInterval &mask, BitInterval &result) {
			/*
			 * interval | vector | dnc
			 * ---------+--------+-------
			 * 00--00   | 000000 | 001100
			 * 1-1-1-   | 101010 | 010101
			 * 1--010   | 100010 | 011000
			 *
			 * result:
			 *  set zeros at: --0---
			 *  set ones  at: -1-1-1
			 *
			 *
			 * zeros:
			 *  use sets:  v | dnc
			 *             0 | 1
			 *             1 | 0
			 *  zeros = &(vec(v) | dnc(v))
			 *  zeros = (000000 | 001100) & (101010 | 010101) & (100010 | 011000)
			 *  zeros =   001100
			 *          & 111111
			 *          & 111010
			 *        =   001000
			 *
			 * ones:
			 *  use sets: v | dnc
			 *            0 | 0
			 *            0 | 1
			 *  ones = &(~(vec(v))
			 *  ones = ~000000 & ~101010 & ~100010
			 *  ones =   111111
			 *         & 010101
			 *         & 011101
			 *       =   010101
			 *
			 * zerosInterval = (000000, ~zeros) = (000000, 110111) = --0---
			 * onesInterval = (ones, ~ones) = (010101, 101010) = -1-1-1
			 *
			 * но нужно зафиксировать только одну переменную, поэтому выставлен следующий приоритет:
			 * 1) если вес zeros == 1, то устанавливаем 0 по заданной компоненте
			 * 2) если вес ones == 1, то устанавливаем 1 по заданной компоненте
			 * 3) если вес zeros > 1, то выбираем любую компоненту и устанавливаем 0
			 * 4) если вес ones > 1, то выбираем любую компоненту и устанавливаем 1
			 * */

			const BitVector zero{mask.getLength()};
			const auto one = ~zero;
			auto zeros{one}, ones{one};

			for (const auto &it : dnf) {
				if (it) {
					auto i = *it & mask;
					zeros &= i.getVector() | i.getDnc();
					ones &= ~i.getVector();
				}
			}

			bool solutionChanged{true};
			if (auto zw = zeros.getWeight(); zw == 1) {
				result |= BitInterval(zero, ~zeros);
			} else if (auto ow = ones.getWeight(); ow == 1) {
				result |= BitInterval(ones, ~ones);
			} else if (zw > 1) {
				BitVector zeroMask{zeros.getLength()};
				// выбираем самую "правую" компоненту
				for (zeroMask.set1(0); (zeroMask & zeros) == zero; zeroMask <<= 1);
				result |= BitInterval(zero, ~zeroMask);
			} else if (ow > 1) {
				BitVector onesMask{ones.getLength()};
				// выбираем самую "правую" компоненту
				for (onesMask.set1(0); (onesMask & ones) == zero; onesMask <<= 1);
				result |= BitInterval(onesMask, ~onesMask);
			} else {
				solutionChanged = false;
			}

			// по скольку у нас могло получиться новое решение,
			// то стоит и сразу удалить ортогональные к нему строки
			return solutionChanged && removeOrthogonal(dnf, mask, result) == 0;
		}

	}

}
