//
// Created by egor9814 on 17 Aug 2021.
//

#include "impl.hpp"

namespace booleq::d0::impl {

	BranchPtr Branch::fix(size_t var, Branch::FixType fixType) {
		return std::make_shared<Branch>(Branch {
				dnf,
				mask,
				solution,
				var,
				fixType
		});
	}

	BranchPtr Branch::create(const RawDNF &dnf, size_t inputsCount) {
		BitVector zero{inputsCount};
		auto one{~zero};
		BitInterval mask{one, zero};
		BitInterval solution{zero, one};

		return std::make_shared<Branch>(Branch {
				dnf,
				mask,
				solution,
				inputsCount,
				FixNone
		});
	}

}
