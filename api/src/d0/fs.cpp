//
// Created by egor9814 on 20 Aug 2021.
//

#include "impl.hpp"

namespace booleq::d0::impl {

	struct OneZeroXFS : public ForkStrategyImpl {
		void fork(const BranchPtr &branch, size_t var, Branches &branches) override {
			// ветвимся по знаку '-' (пропускаем одну переменную)
			branches.push(branch->fix(var, Branch::FixX));
			// ветвимся по '0'
			branches.push(branch->fix(var, Branch::FixZero));

			// фиксируем '1'
			branch->fixType = Branch::FixOne;
			branch->var = var;
		}
	};

	struct OneXZeroFS : public ForkStrategyImpl {
		void fork(const BranchPtr &branch, size_t var, Branches &branches) override {
			// ветвимся по '0'
			branches.push(branch->fix(var, Branch::FixZero));

			// ветвимся по знаку '-' (пропускаем одну переменную)
			branches.push(branch->fix(var, Branch::FixX));

			// фиксируем '1'
			branch->fixType = Branch::FixOne;
			branch->var = var;
		}
	};

	struct ZeroOneXFS : public ForkStrategyImpl {
		void fork(const BranchPtr &branch, size_t var, Branches &branches) override {
			// ветвимся по знаку '-' (пропускаем одну переменную)
			branches.push(branch->fix(var, Branch::FixX));

			// ветвимся по '1'
			branches.push(branch->fix(var, Branch::FixOne));

			// фиксируем '0'
			branch->fixType = Branch::FixZero;
			branch->var = var;
		}
	};

	struct ZeroXOneFS : public ForkStrategyImpl {
		void fork(const BranchPtr &branch, size_t var, Branches &branches) override {
			// ветвимся по '1'
			branches.push(branch->fix(var, Branch::FixOne));

			// ветвимся по знаку '-' (пропускаем одну переменную)
			branches.push(branch->fix(var, Branch::FixX));

			// фиксируем '0'
			branch->fixType = Branch::FixZero;
			branch->var = var;
		}
	};

	struct XOneZeroFS : public ForkStrategyImpl {
		void fork(const BranchPtr &branch, size_t var, Branches &branches) override {
			// ветвимся по '0'
			branches.push(branch->fix(var, Branch::FixZero));

			// ветвимся по '1'
			branches.push(branch->fix(var, Branch::FixOne));

			// фиксируем '-' (пропускаем одну переменную)
			branch->fixType = Branch::FixX;
			branch->var = var;
		}
	};

	struct XZeroOneFS : public ForkStrategyImpl {
		void fork(const BranchPtr &branch, size_t var, Branches &branches) override {
			// ветвимся по '1'
			branches.push(branch->fix(var, Branch::FixOne));

			// ветвимся по '0'
			branches.push(branch->fix(var, Branch::FixZero));

			// фиксируем '-' (пропускаем одну переменную)
			branch->fixType = Branch::FixX;
			branch->var = var;
		}
	};

	std::shared_ptr<ForkStrategyImpl> ForkStrategyImpl::create(ForkStrategy::Enum forkStrategy) {
		switch (forkStrategy) {
			case ForkStrategy::OneZeroX:
				return std::make_shared<OneZeroXFS>();
			case ForkStrategy::OneXZero:
				return std::make_shared<OneXZeroFS>();
			case ForkStrategy::ZeroOneX:
				return std::make_shared<ZeroOneXFS>();
			case ForkStrategy::ZeroXOne:
				return std::make_shared<ZeroXOneFS>();
			case ForkStrategy::XOneZero:
				return std::make_shared<XOneZeroFS>();
			case ForkStrategy::XZeroOne:
				return std::make_shared<XZeroOneFS>();
			default:
				return nullptr;
		}
	}

}
