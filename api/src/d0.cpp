//
// Created by egor9814 on 06 Aug 2021.
//

#include <booleq/d0.hpp>
#include <memory>
#include <stack>
#include <cstring>
#include "d0/impl.hpp"

namespace booleq::d0 {

	using namespace bitops;
	using namespace pla;
	using Impl = impl::RootFinderImpl;

	ForkStrategy::Enum ForkStrategy::parse(const char *shortString, bool *ok) {
		unsigned char result{OneZeroX};
		for (const auto &it : shortStrings) {
			if (strcmp(it, shortString) == 0) {
				if (ok) *ok = true;
				break;
			}
			result++;
		}
		return static_cast<Enum>(result);
	}

	RootFinder::ConjunctionPtr
	RootFinder::findRoot(const pla::RawDNF &dnf, size_t inputsCount, ForkStrategy::Enum forkStrategy) {
		// если кол-во переменных равно 0 или кол-во коньюнкций равно 0, то искать нечего
		if (inputsCount == 0 || dnf.empty())
			// поэтому корня не существует
			return nullptr;

		if (auto branch = Impl::find(dnf, inputsCount, forkStrategy); branch) {
			return std::make_shared<BitInterval>(branch->solution);
		}

		return nullptr;
	}

	RootFinder::ConjunctionPtr RootFinder::trySimplify(const RawDNF &dnf, const RootFinder::ConjunctionPtr &c) {
		if (!c || dnf.empty())
			return nullptr;

		auto len = c->getLength();
		// в качестве рузультата возмем сначала копию исходного корня
		auto result = std::make_shared<BitInterval>(*c);
		// последовательно идем по переменным корня
		for (size_t i = 0; i < len; i++) {
			if (auto bit = result->get(i); bit != BitInterval::TriBit::X) {
				// если текущая переменная не '-', то заменяем её на '-'
				result->set(i, BitInterval::TriBit::X);
				// и проверяем решение
				if (!checkSolution(dnf, result)) {
					// если полученный корень не является решением, то возвращаем предыдущее значение
					result->set(i, bit);
				}
			}
		}
		// если исходный корень совпадает с результирующим, то упростить не удалось
		if (*result == *c)
			return nullptr;
		return result;
	}

	bool RootFinder::checkSolution(const pla::RawDNF &dnf, const ConjunctionPtr &c) {
		// если решения не существует или кол-во переменных равно 0 или кол-во коньюнкций равно 0,
		// то проверять нечего
		if (!c || dnf.empty())
			return false;

		return Impl::checkSolution(dnf, *c);
	}

}
