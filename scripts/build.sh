#!/usr/bin/env bash

if (command -v clang++ &>/dev/null); then
  _cxx=clang++
elif (command -v g++ &>/dev/null); then
  _cxx=g++
else
  echo "C++ compiler not found"
  exit 1
fi

if (command -v ninja &>/dev/null); then
  _gen="-GNinja"
else
  _gen=""
fi

_selfpath=$(realpath -P $0)
_selfdir=$(dirname $_selfpath)

_project=$(dirname $_selfdir)
_build="$_project/build/release"
_art="$_project/artifacts"
mkdir -p $_build
mkdir -p $_art
rm -rf $_art/*

echo ">>> building..."

mkdir -p $_build && echo ">>> output dir: $_build" || exit 1

cmake $_gen \
  -DCMAKE_CXX_COMPILER=$_cxx \
  -DCMAKE_BUILD_TYPE=Release \
  -B $_build \
  $_project && cmake --build $_build && cp -v $_build/booleq $_art/ && echo ">>> finished" || echo ">>> build failed" && exit 1
