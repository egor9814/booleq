
include(FetchContent)

function(git NAME REPO TAG SOURCE_SUBDIR)
    if (NOT ("${SOURCE_SUBDIR}" STREQUAL ""))
        set(SOURCE_SUBDIR "SOURCE_SUBDIR ${SOURCE_SUBDIR}")
    endif()
#    message(STATUS "${NAME}")
#    message(STATUS "${REPO}")
#    message(STATUS "${TAG}")
#    message(STATUS "${SOURCE_SUBDIR}")
    FetchContent_Declare(
            ${NAME}
            GIT_REPOSITORY ${REPO}
            GIT_TAG ${TAG}
            ${SOURCE_SUBDIR}
    )
    FetchContent_MakeAvailable(${NAME})
endfunction(git)
