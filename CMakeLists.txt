cmake_minimum_required(VERSION 3.19)
project(booleq CXX)

add_subdirectory(libs)

file(GLOB_RECURSE runtime_sources "src/*.cpp")
add_executable(booleq ${runtime_sources})
target_compile_features(booleq PUBLIC cxx_std_20)

add_subdirectory(api)
booleq_api_link_target(booleq)

flag_link_target(booleq)

if (${CMAKE_BUILD_TYPE} STREQUAL "Debug")
    add_subdirectory(tests)
endif()
