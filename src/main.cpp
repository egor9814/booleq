#include <iostream>
#include <fstream>
#include <sstream>
#include <set>
#include <flag.hpp>
#include <booleq/d0.hpp>
#include <pla.hpp>

/** Анализ файлов
 * @param files набор путей до PLA файлов
 * @return код ошибки */
int analyze(const std::set<std::string> &files, booleq::d0::ForkStrategy::Enum forkStrategy) {
	using namespace booleq::d0;

	for (const auto &path : files) {
		pla::File pla;
		try {
			// открываем pla файл на чтение
			std::ifstream input{path};
			if (!input) throw std::runtime_error("cannot open file: " + path);
			try {
				// пытаемся прочитать
				input >> pla;
				// закрываем
				input.close();
			} catch (...) {
				// не важно при какой ошибке, но файл все равно закрыть нужно
				if (input.is_open())
					input.close();
				throw;
			}
		} catch (const std::exception &err) {
			std::cout << "error: " << err.what() << std::endl;
			return 2;
		} catch (...) {
			std::cout << "unhandled error" << std::endl;
			return 2;
		}

		// для каждого выходного интервала найдем решение
		for (const auto &[output, dnf] : pla.toDNFs()) {
			std::cout << "finding first solution for '" << path << " -> " << output.toString() << "'" << std::endl;
			// создаем днф из "сырых" указателей на интервалы
			pla::RawDNF rawDnf{dnf.size(), nullptr};
			auto i = dnf.begin();
			auto end = dnf.end();
			// заполняем
			for (auto j = rawDnf.begin(); i != end; ++i, ++j) {
				*j = &(*i);
			}
			// находим решение
			auto solution = RootFinder::findRoot(rawDnf, pla.inputLabels.size(), forkStrategy);
			// проверяем решение
			if (RootFinder::checkSolution(rawDnf, solution)) {
				std::cout << "\tsolution: " << solution->toString() << std::endl;
				// пытаемся упростить
				if (auto simplified = RootFinder::trySimplify(rawDnf, solution); simplified) {
					std::cout << "\tsimplified solution: " << simplified->toString() << std::endl;
				}
			} else {
				std::cout << "\tsolution not found" << std::endl;
			}
		}
	}

	return 0;
}

/** Интерактивный режим
 * @return код ошибки */
int interactive(booleq::d0::ForkStrategy::Enum forkStrategy) {
	std::string path;
	// приветсвие
	std::cout << "Welcome to BoolEq Interactive Shell!" << std::endl;
	std::cout << " Press Ctrl-C for exit." << std::endl;

	// просим пользователя ввести путь до файла
	std::cout << "enter *.pla path> ";
	std::cin >> path;

	// отдаем файл на анализ
	return analyze({path}, forkStrategy);
}

std::string supportedForkStrategies() {
	using namespace booleq::d0::ForkStrategy;
	std::ostringstream out;
	auto it = shortStrings.begin();
	auto end = shortStrings.end();
	if (it != end)
		out << *it++;
	while (it != end) {
		out << ", ";
		out << *it++;
	}
	return out.str();
}

/** Точка входа
 * @return код ошибки */
int main(int argc, char **argv) {
	using namespace booleq::d0;

	if (argc == 1) {
		// если не указаны аргументы командной строки, то запустим интерактивный режим
		return interactive(booleq::d0::ForkStrategy::OneZeroX);
	}

	// набор входных файлов
	std::set<std::string> inputFiles;

	// создаем флаги для обработки аргументов командной строки
	flag::FlagSet flags{argv[0], flag::FlagSet::ErrorHandling::ContinueOnError};

	// добаляем обработчик флага "-i"
	flags.func("i", "Add \"`filepath`\" to analyze list", [&inputFiles](const std::string &s) {
		if (!inputFiles.contains(s))
			inputFiles.insert(s);
	});

	// дабавляем флаг выбора стратегии ветвления
	auto forkStrategyVar = flags.newString("fs", *ForkStrategy::shortStrings.begin(),
										   R"(Set on of ")" + supportedForkStrategies() + R"(" `"fork strategy"`)");

	if (auto err = flags.parse(argc, argv); !err.empty()) {
		std::cerr << err << std::endl;
		return 1;
	}

	bool success{false};
	auto forkStrategy = ForkStrategy::parse(forkStrategyVar->data(), &success);
	if (!success) {
		forkStrategy = ForkStrategy::OneZeroX;
		std::cout << "warning: unknown fork strategy \"" + *forkStrategyVar + R"(", used default "10-")" << std::endl;
	}

	if (inputFiles.empty()) {
		return interactive(forkStrategy);
	} else {
		return analyze(inputFiles, forkStrategy);
	}
}
