# booleq

Решатель логических уравнений

## Работа с программой
1) Запустить из консоли без параметров и ввести путь до файла
2) Запустить из консоли с параметром ```-i``` и путь до файла:
    - ```booleq -i path/to/pla/file.pla```

## Сборка на Linux, *BSD, macOS
Зависимости:
- cmake 3.19
- clang 12 (с поддержкой c++20)
- ninja 1.10 (опционально)

Можно воспользоваться скриптом в терминале:
- ```$ ./scripts/build.sh```

После чего исполняемый файл будет находится в папке ```artifacts```

Второй способ: выполнить следующую последовательность комманд в терминале:
```
$ mkdir .build && cd .build
$ cmake ..
$ cmake --build .
```
И для запуска: ```$ ./booleq```

## Сборка на Windows
Зависимости:
- Microsoft PowerShell 7
- cmake 3.19
- clang 12 (с поддержкой c++20)
- ninja 1.10

Установка зависимостей:
1) Установка [Microsoft PowerShell 7](https://docs.microsoft.com/ru-ru/powershell/scripting/install/installing-powershell-core-on-windows?view=powershell-7.1)
2) Установка [Scoop](https://scoop.sh/) в Microsoft PowerShell 7:
    - ```PS > iwr -useb get.scoop.sh | iex```
3) Установка необходимых пакетов в Microsoft PowerShell 7:
    - ```PS > scoop install cmake llvm ninja```


Для сборки выполнить следующую последовательность комманд в Microsoft PowerShell 7:
```
PS > mkdir .build && cd .build
PS > cmake -GNinja ..
PS > cmake --build .
```
И для запуска: ```PS > .\booleq```
